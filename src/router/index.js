import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store';
import HomeView from '../views/HomeView.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
  },
  {
    path: '/weather',
    name: 'weather',
    component: () => import('../views/WeatherView.vue'),
    meta: { requiresAuth: true },
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

router.beforeEach((to, from, next) => {
  const isAuthed = store.getters['auth/isAuthed'];
  if (to.meta.requiresAuth) {
    if (!isAuthed) {
      next('/');
    } else {
      next();
    }
  } else if (isAuthed) {
    next('/weather');
  } else {
    next();
  }
});

export default router;
