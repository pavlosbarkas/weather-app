import Vue from 'vue';
import Vuex from 'vuex';
import auth from '@/store/modules/auth';
import weather from '@/store/modules/weather';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth,
    weather,
  },
});
