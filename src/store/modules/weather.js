import axios from 'axios';

export default {
  namespaced: true,
  state: {
    coordinates: null,
    loading: false,
    weatherData: null,
  },
  mutations: {
    setCoordinates(state, coordinates) {
      state.coordinates = coordinates;
    },
    setLoading(state, loading) {
      state.loading = loading;
    },
    setWeather(state, weatherData) {
      state.weatherData = weatherData;
    },
  },
  actions: {
    getCityCoordinates: async ({ commit }, city) => {
      const API_URL = `https://api.openweathermap.org/geo/1.0/direct?q=${city}&limit=1&appid=${process.env.VUE_APP_WEATHER_API_KEY}`;

      try {
        commit('setLoading', true);
        const response = await axios.get(API_URL);
        commit('setCoordinates', response.data[0]);
        return Promise.resolve();
      } catch (e) {
        return Promise.reject(e);
      } finally {
        commit('setLoading', false);
      }
    },
    getWeatherData: async ({ state, commit }) => {
      const API_URL = `https://api.openweathermap.org/data/3.0/onecall?lat=${state.coordinates?.lat}&lon=${state.coordinates?.lon}&appid=${process.env.VUE_APP_WEATHER_API_KEY}&units=metric`;

      try {
        commit('setLoading', true);
        const response = await axios.get(API_URL);
        commit('setWeather', response.data.current);
        return Promise.resolve();
      } catch (e) {
        return Promise.reject(e);
      } finally {
        commit('setLoading', false);
      }
    },
    resetState: ({ commit }) => {
      commit('setCoordinates', null);
      commit('setLoading', false);
      commit('setWeather', null);
    },
  },
  getters: {
    getLoading: (state) => state.loading,
    getWeatherData: (state) => state.weatherData,
  },
};
