export default {
  namespaced: true,
  state: {
    isAuthed: !!localStorage.getItem('isAuthed'),
  },
  mutations: {
    setIsAuthed: (state, authed) => {
      state.isAuthed = authed;
    },
  },
  actions: {
    login({ commit }, { email, password }) {
      if (email === 'weather.test@mail.com' && password === '123456') {
        localStorage.setItem('isAuthed', 'true');
        commit('setIsAuthed', true);
        return Promise.resolve();
      }
      throw new Error('Invalid credentials');
    },
    logout({ commit }) {
      localStorage.removeItem('isAuthed');
      commit('setIsAuthed', false);
    },
  },
  getters: {
    isAuthed: (state) => state.isAuthed,
  },
};
